output "public_ip" {
  value       = module.test_task.public_ip
  description = "Public IP Address of EC2 instance"
}

output "instance_id" {
  value       = module.test_task.instance_id
  description = "Instance ID"
}
