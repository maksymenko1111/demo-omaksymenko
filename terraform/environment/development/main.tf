module "test_task" {
  source = "../../modules/test-task"

  environment   = var.environment
  project_name  = var.project_name
  ami_server    = data.aws_ami.debian_ami.image_id
  key_name      = var.key_name
  instance_type = var.instance_type
}

data "aws_ami" "debian_ami" {
  most_recent = true
  owners      = ["136693071363"]

  filter {
    name   = "name"
    values = ["debian-12-amd64-2024*"]
  }
}
