terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.35.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.1"
    }
  }

  backend "http" {
  }
}

provider "aws" {
  region = "eu-central-1"
}
