variable "environment" {
  type        = string
  description = "The name of the environemnts"
}

variable "project_name" {
  type        = string
  description = "The name of the project"
  default     = "test-task-obrio"
}

variable "key_name" {
  type        = string
  description = "The key name to the server"
  default     = "gitlab"
}

variable "instance_type" {
  type        = string
  description = "The instance type of the server"
  default     = "t2.micro"
}
