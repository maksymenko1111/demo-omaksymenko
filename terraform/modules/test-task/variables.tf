variable "environment" {
  type        = string
  description = "The name of the environemnts"
}

variable "project_name" {
  type        = string
  description = "The name of the project"
}

variable "ami_server" {
  type        = string
  description = "The AMI for the server"
}

variable "key_name" {
  type        = string
  description = "The key name to the server"
}

variable "instance_type" {
  type        = string
  description = "The instance type of the server"
}
