locals {
  name = "node-server-${var.environment}"
}

resource "aws_instance" "ec2_server" {
  ami           = var.ami_server
  instance_type = var.instance_type

  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.ec2_server.id]
  iam_instance_profile        = aws_iam_instance_profile.profile.id
  key_name                    = var.key_name
  user_data                   = <<EOF
#!/bin/bash
for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do apt-get remove $pkg; done
apt-get update -y
apt-get install ca-certificates curl -y
install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
chmod a+r /etc/apt/keyrings/docker.asc
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update -y
apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y
EOF

  tags = {
    Name        = local.name
    Project     = var.project_name
    Environment = var.environment
  }
}

resource "aws_security_group" "ec2_server" {
  name = local.name

  ingress {
    description = "ssh port"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "node port"
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = local.name
    Project     = var.project_name
    Environment = var.environment
  }
}
