
--
-- Table structure for table `visitors`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE IF NOT EXISTS `visitors` (
  `id` MEDIUMINT NOT NULL AUTO_INCREMENT,
  `user` varchar(100) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visitors`
--

LOCK TABLES `visitors` WRITE;
/*!40000 ALTER TABLE `visitors` DISABLE KEYS */;
INSERT INTO `visitors` VALUES (1,'Jimmy Page'),(2,'Robert Plant'),(3,'John Paul Jones'),(4,'John Bonham');
/*!40000 ALTER TABLE `visitors` ENABLE KEYS */;
UNLOCK TABLES;