#!/bin/bash

mysql_user="nodeuser"
mysql_password=$1
mysql_root_password=$2

mysql_database="visitors"
table_name="visitors"

sudo apt update -y
sudo apt upgrade -y

export DEBIAN_FRONTEND=noninteractive
echo "mysql-server-8.0 mysql-server/root_password password ${mysql_root_password}" > mysql_preseed.txt
echo "mysql-server-8.0 mysql-server/root_password_again password ${mysql_root_password}" >> mysql_preseed.txt
sudo debconf-set-selections < mysql_preseed.txt

wget https://dev.mysql.com/get/mysql-apt-config_0.8.29-1_all.deb
sudo apt-get -y install gnupg
sudo -E dpkg -i mysql-apt-config_0.8.29-1_all.deb
sudo apt update -y
sudo -E apt-get -y install mysql-server

sudo systemctl start mysql

if sudo mysql -e "SHOW DATABASES;" | grep -q $mysql_database; then
    echo "Database $mysql_database already exist."
else
    sudo mysql -e "CREATE DATABASE visitors"
    sudo mysql visitors < dump.sql 
    sudo mysql -e "CREATE USER '${mysql_user}'@'%' IDENTIFIED BY '${mysql_password}'; GRANT ALL PRIVILEGES ON ${mysql_database}.* TO '${mysql_user}'@'%'; FLUSH PRIVILEGES;"
fi

sudo sh -c 'echo "bind-address = 0.0.0.0" >> /etc/mysql/mysql.conf.d/mysqld.cnf'
rm -rf mysql_preseed.txt

